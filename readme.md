# Learning Portal Backend

Our open source project is initially inspired by our passion for digitalization of organization in BINUS University.

It is a learning management system that hopefully can optimize efficiency of organizations in managing their learning classes.

Our open source project focuses on four aspects: course management, attendance management, project assessment, documentation and reports.

We set up three different users, namely members, teachers, and admin.

Sample program is run here : [leapbe-os.bncc.net](https://leapbe-os.bncc.net)

## System Requirements

-   PHP >= 7.2.0
-   BCMath PHP Extension
-   Ctype PHP Extension
-   JSON PHP Extension
-   Mbstring PHP Extension
-   OpenSSL PHP Extension
-   PDO PHP Extension
-   Tokenizer PHP Extension
-   XML PHP Extension

## Installation

1. Use the package manager [composer](https://getcomposer.org/download/)

    **composer must be version 1.10.22**

```bash
composer install
```

2. Copy .env file from .env.example
3. Configure .env according to your environment settings, for example:

```bash
DB_DATABASE=[DATABASE_NAME]
DB_USERNAME=[DATABASE_USERNAME]
DB_PASSWORD=[DATABASE_PASSWORD]
```

4. Generate application key

```bash
php artisan key:generate
```

5. Migrate the database

```bash
php artisan migrate:fresh
```

We also provide a sample seeder located in DatabaseSeeder.php. If you wish to run it, use the command

```bash
php artisan migrate:fresh --seed
```

before migrating, or

```bash
php artisan db:seed
```

if you have already migrated the database

6. Install Passport

```bash
php artisan passport:install
```

7. Link storage/app folder to public folder

```bash
php artisan storage:link
```

## 🛠 Technologies

This project is created with:

-   Laravel PHP Framework: 6.0

## 🤝 Contributing

Feel free to contribute on this project by following the guideline below!

-   Fork this repository

-   Create merge request to master branch

-   Fill the title with your changes

-   Fill the description and describe your changes

## ⚙️ Submitting Issue

When you find some issue, you can submit a issue to help this project

-   Fill the title with the brief problem

-   Describe the problem and how to reproduce it

-   Alternatively, you can suggest some of your ideas to solve the problem

## 💻 Another Project to contribute

If you are interested to contribute to our frontend project, you can check [here](https://gitlab.com/rnddev/leap-fe).

## ⚖️ Licence

leap-be is [MIT Licenced](https://gitlab.com/rnddev/leap-be/-/blob/master/LICENCE)
