<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Absence;
use Faker\Generator as Faker;

$factory->define(Absence::class, function (Faker $faker) {
    return [
        'absence' => $faker->numberBetween(0,1),
        'class_detail_id' => $faker->numberBetween(1,40),
        'class_schedule_id' => $faker->numberBetween(1,20)
    ];
});
