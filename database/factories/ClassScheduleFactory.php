<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ClassSchedule;
use App\UserClass;
use Faker\Generator as Faker;

$factory->define(ClassSchedule::class, function (Faker $faker) {
    $classes= UserClass::all()->pluck('id')->toArray();
    $date = $faker->dateTimeBetween('+1 week','+1 month');
    return [
        'user_class_id'=>$faker->randomElement($classes),
        'shift_id' => $faker->numberBetween($min = 1, $max = 4),
        'meeting_date' => $date,
        'curriculum_id' =>$faker->numberBetween(1,20),
        'day' => "AAAA"
    ];
});
