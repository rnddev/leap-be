<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Score;
use Faker\Generator as Faker;

$factory->define(Score::class, function (Faker $faker) {
    return [
        'class_detail_id' => $faker->numberBetween($min=1, $max=40),
        'mid_score' => $faker->numberBetween($min=0, $max = 100),
        'final_score' => $faker->numberBetween($min=0, $max = 100),
    ];
});
