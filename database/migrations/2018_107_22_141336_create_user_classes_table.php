<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_classes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('period');
            $table->bigInteger('term_id')->unsigned();
            $table->bigInteger('course_id')->unsigned();
            $table->bigInteger('location_id')->unsigned();
            $table->bigInteger('lecturer_id')->unsigned();
            $table->string('master_day');
            $table->bigInteger('master_shift_id')->unsigned();
            $table->integer('capacity');
            $table->timestamps();

            $table->foreign('term_id')->references('id')->on('terms')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('lecturer_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('master_shift_id')->references('id')->on('shifts')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_classes');
    }
}
