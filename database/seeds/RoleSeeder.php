<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Role::class)->create([
            'role_name' => 'admin'
        ]);
        factory(\App\Role::class)->create([
            'role_name' => 'lecturer'
        ]);
        factory(\App\Role::class)->create([
            'role_name' => 'member'
        ]);
    }
}
