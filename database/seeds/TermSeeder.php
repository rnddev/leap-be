<?php

use Illuminate\Database\Seeder;

class TermSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('terms')->insert([
            'term_name' => "Odd Semester"
        ]);

        DB::table('terms')->insert([
            'term_name' => "Even Semester"
        ]);
    }
}
