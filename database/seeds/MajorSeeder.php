<?php

use Illuminate\Database\Seeder;
use App\Major;

class MajorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('majors')->insert([
            'major_name'=>"Teknik Informatika"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Sistem Informasi"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Desain Komunikasi Visual"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Others"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Sistem Komputer"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Teknik Informatika & Matematika"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Manajemen"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Game Application Technology"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Sistem Informasi & Manajemen"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Akuntansi & Sistem Informasi"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Teknik Industri"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Mobile Application Technology"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Akuntansi"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"International Marketing"
        ]);
        
        DB::table('majors')->insert([
            'major_name'=>"Komunikasi Pemasaran"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Komputerisasi Akuntansi"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Psikologi"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Sastra Jepang"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Sastra Cina"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Sastra Inggris"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Teknik Industri & Sistem Informasi"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Arsitektur"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Teknik Sipil"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Information System Audit"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Desain Interior"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Teknik Informatika & Statistika"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Teknik Industri & Manajamen"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Cyber Security"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Magister Manajemen Sistem Informasi"
        ]);

        DB::table('majors')->insert([
            'major_name'=>"Magister Teknik Informatika"
        ]);
    }
}
