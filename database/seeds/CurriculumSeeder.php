<?php

use Illuminate\Database\Seeder;
use App\Curriculum;

class CurriculumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('curricula')->insert([
            'course_id'=>"1",
            'term_id'=>"1",
            'meeting_name'=>"Meeting 1",
            'meeting'=>"1",
            'material'=>"1/1/1/1_1_1_Meeting 1_1575812263.pdf",
        ]);

        DB::table('curricula')->insert([
            'course_id'=>"1",
            'term_id'=>"1",
            'meeting_name'=>"Meeting 2",
            'meeting'=>"2",
            'material'=>"1/1/2/1_1_2_Meeting 2_1575812263.pdf",
        ]);

        DB::table('curricula')->insert([
            'course_id'=>"1",
            'term_id'=>"1",
            'meeting_name'=>"Meeting 3",
            'meeting'=>"3",
            'material'=>"1/1/3/1_1_3_Meeting 3_1575812263.pdf",
        ]);

        DB::table('curricula')->insert([
            'course_id'=>"1",
            'term_id'=>"1",
            'meeting_name'=>"Meeting 4",
            'meeting'=>"4",
            'material'=>"1/1/4/1_1_4_Meeting 4_1575812263.pdf",
        ]);

        DB::table('curricula')->insert([
            'course_id'=>"1",
            'term_id'=>"1",
            'meeting_name'=>"Meeting 5",
            'meeting'=>"5",
            'material'=>"1/1/5/1_1_5_Meeting 5_1575812263.pdf",
        ]);

        DB::table('curricula')->insert([
            'course_id'=>"1",
            'term_id'=>"1",
            'meeting_name'=>"Meeting 6",
            'meeting'=>"6",
            'material'=>"1/1/6/1_1_6_Meeting 6_1575812263.pdf",
        ]);

        DB::table('curricula')->insert([
            'course_id'=>"1",
            'term_id'=>"2",
            'meeting_name'=>"Meeting 7",
            'meeting'=>"7",
            'material'=>"1/2/7/1_2_7_Meeting 7_1575812263.pdf",
        ]);

        DB::table('curricula')->insert([
            'course_id'=>"1",
            'term_id'=>"2",
            'meeting_name'=>"Meeting 8",
            'meeting'=>"8",
            'material'=>"1/2/8/1_2_8_Meeting 8_1575812263.pdf",
        ]);

        DB::table('curricula')->insert([
            'course_id'=>"1",
            'term_id'=>"2",
            'meeting_name'=>"Meeting 9",
            'meeting'=>"9",
            'material'=>"1/2/9/1_2_9_Meeting 9_1575812263.pdf",
        ]);

        DB::table('curricula')->insert([
            'course_id'=>"1",
            'term_id'=>"2",
            'meeting_name'=>"Meeting 10",
            'meeting'=>"10",
            'material'=>"1/2/10/1_2_10_Meeting 10_1575812263.pdf",
        ]);



        DB::table('curricula')->insert([
            'course_id'=>"2",
            'term_id'=>"1",
            'meeting_name'=>"Meeting 1",
            'meeting'=>"1",
            'material'=>"2/1/1/2_1_1_Meeting 1_1575813000.pdf",
        ]);

        DB::table('curricula')->insert([
            'course_id'=>"2",
            'term_id'=>"1",
            'meeting_name'=>"Meeting 2",
            'meeting'=>"2",
            'material'=>"2/1/2/2_1_2_Meeting 2_1575813000.pdf",
        ]);

        DB::table('curricula')->insert([
            'course_id'=>"2",
            'term_id'=>"1",
            'meeting_name'=>"Meeting 3",
            'meeting'=>"3",
            'material'=>"2/1/3/2_1_3_Meeting 3_1575813000.pdf",
        ]);

        DB::table('curricula')->insert([
            'course_id'=>"2",
            'term_id'=>"1",
            'meeting_name'=>"Meeting 4",
            'meeting'=>"4",
            'material'=>"2/1/4/2_1_4_Meeting 4_1575813000.pdf",
        ]);

        DB::table('curricula')->insert([
            'course_id'=>"2",
            'term_id'=>"1",
            'meeting_name'=>"Meeting 5",
            'meeting'=>"5",
            'material'=>"2/1/5/2_1_5_Meeting 5_1575813000.pdf",
        ]);

        DB::table('curricula')->insert([
            'course_id'=>"2",
            'term_id'=>"1",
            'meeting_name'=>"Meeting 6",
            'meeting'=>"6",
            'material'=>"2/1/6/2_1_6_Meeting 6_1575813000.pdf",
        ]);

        DB::table('curricula')->insert([
            'course_id'=>"2",
            'term_id'=>"2",
            'meeting_name'=>"Meeting 7",
            'meeting'=>"7",
            'material'=>"2/2/7/2_2_7_Meeting 7_1575813000.pdf",
        ]);

        DB::table('curricula')->insert([
            'course_id'=>"2",
            'term_id'=>"2",
            'meeting_name'=>"Meeting 8",
            'meeting'=>"8",
            'material'=>"2/2/8/2_2_8_Meeting 8_1575813000.pdf",
        ]);

        DB::table('curricula')->insert([
            'course_id'=>"2",
            'term_id'=>"2",
            'meeting_name'=>"Meeting 9",
            'meeting'=>"9",
            'material'=>"2/2/9/2_2_9_Meeting 9_1575813000.pdf",
        ]);

        DB::table('curricula')->insert([
            'course_id'=>"2",
            'term_id'=>"2",
            'meeting_name'=>"Meeting 10",
            'meeting'=>"10",
            'material'=>"2/2/10/2_2_10_Meeting 10_1575813000.pdf",
        ]);
    }
}
