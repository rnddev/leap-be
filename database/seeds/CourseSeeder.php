<?php

use Illuminate\Database\Seeder;
use App\Course;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->insert([
            'course_name'=>"Web Design"
        ]);
        DB::table('courses')->insert([
            'course_name'=>"Web Programming"
        ]);
    }
}
