<?php

use Illuminate\Database\Seeder;

class ShiftSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shifts')->insert([
            'begin_time' => "07:20:00",
            'end_time' => "09:00:00"
        ]);

        DB::table('shifts')->insert([
            'begin_time' => "09:20:00",
            'end_time' => "11:00:00"
        ]);

        DB::table('shifts')->insert([
            'begin_time' => "11:20:00",
            'end_time' => "13:00:00"
        ]);
        DB::table('shifts')->insert([
            'begin_time' => "13:20:00",
            'end_time' => "15:00:00"
        ]);
    }
}
