<?php

use Illuminate\Database\Seeder;
use App\SubmittedAssignment;

class SubmittedAssignmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('submitted_assignments')->insert([
            'mid_exam_file' => "mid_project/1_member1@member.com_1576000828.pdf",
            'final_exam_file' => "final_project/1_member1@member.com_1576000851.pdf",
            'class_detail_id' => 1
        ]);
    }
}
