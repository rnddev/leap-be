<?php

use Illuminate\Database\Seeder;
use App\Absence;

class AbsenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('absences')->insert([
            'absence' => 1,
            'class_detail_id' => 1,
            'class_schedule_id' => 1
        ]);
        DB::table('absences')->insert([
            'absence' => 0,
            'class_detail_id' => 1,
            'class_schedule_id' => 2
        ]);
        DB::table('absences')->insert([
            'absence' => 0,
            'class_detail_id' => 1,
            'class_schedule_id' => 3
        ]);
        DB::table('absences')->insert([
            'absence' => 1,
            'class_detail_id' => 1,
            'class_schedule_id' => 4
        ]);
    }
}
