<?php

use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        DB::table('users')->insert([
            'name' => $faker->name,
            'user_id' => Str::random(20),
            'email' => 'admin@admin.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'date_of_birth' => now(),
            'major_id' => $faker->numberBetween($min=1, $max=10),
            'gender' => "Male",
            'phone_number' => "000000000000",
            'linkedin' => Str::random(10),
            'instagram_id' => Str::random(10),
            'line_id' => Str::random(10),
            'linkedin' => Str::random(11),
            'role_id' => 1,
            'remember_token' => Str::random(10),
        ]);

        DB::table('users')->insert([
            'name' => $faker->name,
            'user_id' => Str::random(20),
            'email' => 'praet1@praetorian.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'date_of_birth' => now(),
            'major_id' => $faker->numberBetween($min=1, $max=10),
            'gender' => "Male",
            'phone_number' => "000000000000",
            'linkedin' => Str::random(10),
            'instagram_id' => Str::random(10),
            'line_id' => Str::random(10),
            'linkedin' => Str::random(11),
            'role_id' => 2,
            'remember_token' => Str::random(10),
        ]);

        DB::table('users')->insert([
            'name' => $faker->name,
            'user_id' => Str::random(20),
            'email' => 'praet2@praetorian.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'date_of_birth' => now(),
            'major_id' => $faker->numberBetween($min=1, $max=10),
            'gender' => "Male",
            'phone_number' => "000000000000",
            'linkedin' => Str::random(10),
            'instagram_id' => Str::random(10),
            'line_id' => Str::random(10),
            'linkedin' => Str::random(11),
            'role_id' => 2,
            'remember_token' => Str::random(10),
        ]);

        DB::table('users')->insert([
            'name' => $faker->name,
            'user_id' => Str::random(20),
            'email' => 'member1@member.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'date_of_birth' => now(),
            'major_id' => $faker->numberBetween($min=1, $max=10),
            'gender' => "Male",
            'phone_number' => "000000000000",
            'linkedin' => Str::random(10),
            'instagram_id' => Str::random(10),
            'line_id' => Str::random(10),
            'linkedin' => Str::random(11),
            'role_id' => 3,
            'remember_token' => Str::random(10),
            'created_at' => Carbon::createFromDate(2019, 10, 25)
        ]);

        DB::table('users')->insert([
            'name' => $faker->name,
            'user_id' => Str::random(20),
            'email' => 'member2@member.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'date_of_birth' => now(),
            'major_id' => $faker->numberBetween($min=1, $max=10),
            'gender' => "Male",
            'phone_number' => "000000000000",
            'linkedin' => Str::random(10),
            'instagram_id' => Str::random(10),
            'line_id' => Str::random(10),
            'linkedin' => Str::random(11),
            'role_id' => 3,
            'remember_token' => Str::random(10),
            'created_at' => Carbon::createFromDate(2019, 9, 25)
        ]);
    }
}
