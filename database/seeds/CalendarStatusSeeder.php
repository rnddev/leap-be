<?php

use Illuminate\Database\Seeder;

class CalendarStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('calendar_statuses')->insert([
            'status_name'=>"Holiday"
        ]);

        DB::table('calendar_statuses')->insert([
            'status_name'=>"Routine Class"
        ]);

        DB::table('calendar_statuses')->insert([
            'status_name'=>"Register"
        ]);
    }
}
