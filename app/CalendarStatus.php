<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalendarStatus extends Model
{
    protected $fillable = ['status_name'];

    public function academicCalendar(){
        return $this->hasMany('App\AcademicCalendar');
    }
}
