<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassSchedule extends Model
{
    protected $fillable = ['curriculum_id','shift_id','meeting_date','user_class_id','day'];

    public function shift(){
        return $this->belongsTo('App\Shift');
    }

    public function curriculum(){
        return $this->belongsTo('App\Curriculum');
    }

    public function userClass(){
        return $this->belongsTo('App\UserClass');   
    }

    public function absence(){
        return $this->hasOne('App\Absence');
    }
}
