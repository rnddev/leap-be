<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Major extends Model
{
    protected $fillable = ['major_name'];

    public function user(){
        return $this->hasMany('App\User');
    }
}
