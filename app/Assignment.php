<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    protected $fillable=['mid_exam_name','final_exam_name','mid_exam_file','final_exam_file','course_id',
    'final_judgment_criteria','mid_judgment_criteria','final_requirement','mid_requirement'];

    public function course(){
        return $this->belongsTo('App\Course');
    }
}


