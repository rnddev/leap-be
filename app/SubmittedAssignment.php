<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubmittedAssignment extends Model
{
    protected $fillable=['mid_exam_file','final_exam_file','class_detail_id'];

    public function classDetail(){
        return $this->belongsTo('App\ClassDetail');
    }
}
