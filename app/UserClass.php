<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserClass extends Model
{
    protected $fillable = ['period','term_id','location_id','lecturer_id','master_day','master_shift_id','master_shift_id','capacity','course_id'];

    public function user(){
        return $this->belongsTo('App\User','lecturer_id');
    }

    public function score(){
        return $this->hasManyThrough('App\Score','App\ClassDetail');
    }

    public function classDetail(){
        return $this->hasMany('App\ClassDetail');
    }

    public function term() {
        return $this->belongsTo('App\Term');
    }

    public function location() {
        return $this->belongsTo('App\Location');
    }

    public function classSchedule(){
        return $this->hasMany('App\ClassSchedule');
    }

    public function course(){
        return $this->belongsTo('App\Course');
    }

    public function shift(){
        return $this->belongsTo('App\Shift','master_shift_id','id');
    }

    public function SubmittedAssignment()
    {
        return $this->hasManyThrough('App\SubmittedAssignment', 'App\ClassDetail');
    }
}
