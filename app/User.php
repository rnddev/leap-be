<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'email', 'password', 'date_of_birth', 'gender', 'phone_number','linkedin', 'instagram_id', 'line_id', 'major_id', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function hasRole($role)
    {
        return (auth()->user()->role->role_name == $role) ? true : false;
    }

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function userClass()
    {
        return $this->hasMany('App\UserClass','lecturer_id');
    }

    public function classDetail()
    {
        return $this->hasMany('App\ClassDetail');
    }

    public function major()
    {
        return $this->belongsTo('App\Major');
    }
}
