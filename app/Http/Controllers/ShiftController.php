<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shift;
use Validator;
use Carbon\Carbon;

class ShiftController extends Controller
{
    public function getAllShift(){
        $shifts = Shift::select('id','begin_time','end_time')->get();
        $status = 200;
        $message = "OK";

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $shifts
        ]);
    }

    public function getShift(Shift $shift){
        $status = 200;
        $message = "ok";
        if(!$shift){
            $status = 502;
            $message = "Data Not Found";
        }
        return response()->json([
            "status" => $status,
            "message" => $message,
            "data" => $shift->makeHidden(['created_at','updated_at'])
        ]);
    }

    public function createShift(Request $request){
        
        $validator = Validator::make($request->all(),[
            'begin_time' => ['required','regex:/^(([0-1][0-9]|2[0-3]):[0-5][0-9]|24:00)$/'],
            'end_time' => ['required','regex:/^(([0-1][0-9]|2[0-3]):[0-5][0-9]|24:00)$/']
        ]);

        if(!$validator->fails()){
            $begin_time = Carbon::parse($request->begin_time)->format('H:i');
            $end_time = Carbon::parse($request->end_time)->format('H:i');
            $status = 200;
            $message = "Created Shift";
            
            if($begin_time < $end_time){
                $shift = new Shift;
                $shift->begin_time = $begin_time;
                $shift->end_time = $end_time;
                $shift->save(); 
            } else {
                $status = 400;
                $message = "end_time must be greater than begin_time";
                $shift = null;
            }
        } else {
            $status = 400;
            $message = $validator->errors();
            $shift = null;
        }

        
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $shift
        ]);
    }

    public function deleteShift(Shift $shift){
        $status = 200;
        $message = "Deleted Data";
        if(!$shift){
            $status = 400;
            $message = "No Data Found";
        }
        $shift->delete();

        return response()->json([
            "status" => $status,
            "message" => $message
        ]);
    }

    public function editShift(Request $request,Shift $shift){
        $validator = Validator::make($request->all(),[
            'begin_time' => ['regex:/^(([0-1][0-9]|2[0-3]):[0-5][0-9]|24:00)$/'],
            'end_time' => ['regex:/^(([0-1][0-9]|2[0-3]):[0-5][0-9]|24:00)$/']
        ]);
        if(!$validator->fails()){
            $begin_time = $request->begin_time == null ? $shift->begin_time : Carbon::parse($request->begin_time)->format('H:i');
            $end_time = $request->end_time == null ? $shift->end_time : Carbon::parse($request->end_time)->format('H:i');
            $status = 200;
            $message = "Created Shift";
            if($begin_time < $end_time){
                $shift->begin_time = $begin_time;
                $shift->end_time = $end_time;
                $shift->save(); 
            } else {
                $status = 400;
                $message = "end_time must be greater than begin_time";
            }
        } else {
            $status = 400;
            $message = $validator->errors();
        }
        
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $shift
        ]);

    }
}
