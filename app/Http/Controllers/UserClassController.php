<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\ClassScheduleController;
use App\ClassSchedule;
use App\UserClass;
use App\ClassDetail;

class UserClassController extends Controller
{
    protected $classScheduleController;

    public function __construct(){
        $this->classScheduleController = new ClassScheduleController();
    }
    
    public function getAllClass()
    {
        $config = DB::table('configs')->get()->first();
        $WHERE_CONDITION = ['term_id'=>$config->term_id,'period'=>$config->period];
        $classes = $this->getClassWhere($WHERE_CONDITION);
        $classes = $this->countRegisteredMember($classes);
        $status = 200;
        $message = "OK";
        if($classes==null){
            $status = 502;
            $message = "Data Not Found";
        }
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $classes
        ]);
    }

    public function getClassById(UserClass $class)
    {
        $status = 200;
        $message = "OK";
        $WHERE_CONDITION = ['user_classes.id'=>$class->id];
        $class_selected = $this->getClassWhere($WHERE_CONDITION);
        $class_selected = $this->countRegisteredMember($class_selected);
        $class_selected = $this->getMemberInClass($class_selected);
        if(!$class_selected){
            $status = 502;
            $message = "Data Not Found";
        }
        return response()->json([
            "status" => $status,
            "message" => $message,
            "data" => $class_selected
        ]);
    }

    public function getClassByCourse($id){
        $config = DB::table('configs')->get()->first();
        $WHERE_CONDITION = ['term_id'=>$config->term_id,'period'=>$config->period,'course_id'=>$id];
        $classes = $this->getClassWhere($WHERE_CONDITION);
        $classes = $this->countRegisteredMember($classes);
        
        $status = 200;
        $message = "OK";
        if($classes==null){
            $status = 502;
            $message = "Data Not Found";
        }
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $classes
        ]);
    }

    public function getAllMemberClass(){
        $config = DB::table('configs')->get()->first();
        $memberId = auth()->user()->id;
        $WHERE_CONDITION = ['term_id'=> $config->term_id,'period'=>$config->period,'class_details.member_id'=>$memberId];
        $registeredClass = $this->getClassWhere($WHERE_CONDITION);
        
        if($registeredClass==null){
            return $this->index();
        } else {
            $status = 200;
            $message = "OK";
            return response()->json([
                'status' => $status,
                'message' => $message,
                'data' => $registeredClass
            ]);
        }
    }

    public function getAllLecturerClass(){
        $config = DB::table('configs')->get()->first();
        $lecturerId = auth()->user()->id;
        $WHERE_CONDITION = ['term_id'=>$config->term_id,'period'=>$config->period,'lecturer_id'=>$lecturerId];
        $classes = $this->getClassWhere($WHERE_CONDITION);
        $classes = $this->countRegisteredMember($classes);
        $classes = $this->getMemberInClass($classes);
        $status = 200;
        $message = "OK";
        if($classes==null){
            $status = 502;
            $message = "Data Not Found";
        }
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $classes
        ]);
    }
    public function getSubmittedAssignment(UserClass $class){
        return response()->json(["data"=>$class->SubmittedAssignment],200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'period' => 'required|numeric|min:1',
            'term_id' => 'required|numeric|exists:terms,id|min:1',
            'location_id' => 'required|numeric|exists:locations,id|min:1',
            'lecturer_id' => 'required|numeric|exists:users,id|min:1',
            'capacity' => 'required|numeric|min:1',
            'course_id' => 'required|numeric|exists:courses,id|min:1',
            'day' =>  'required',
            'shift_id' => 'required|numeric|exists:shifts,id|min:1'
        ]);
        
        $status = 201;
        $message = "OK";
        
        if($validator->fails()){
            $status = 400;
            $message = $validator->errors();
            return response()->json([
                "status" => $status,
                "error" => $message,
            ]);
        } 
    
        $class = new UserClass;
        $class->period = $request->period;
        $class->term_id = $request->term_id;
        $class->location_id = $request->location_id;
        $class->lecturer_id = $request->lecturer_id;
        $class->capacity = $request->capacity;
        $class->course_id = $request->course_id;
        $class->master_day = $request->day;
        $class->master_shift_id = $request->shift_id;
        $class->created_at = Carbon::now();
        $class->save();
        $class->class_Schedule = 
        $this->classScheduleController->generateClassSchedule($class);
        return response()->json([
            "status" => $status,
            "message" => $message,
            "error" => ""
        ]);
    }

    public function update(Request $request, UserClass $class)
    {
        $validator = Validator::make($request->all(),[
            'period' => 'numeric|min:1',
            'term_id' => 'numeric|exists:terms,id|min:1',
            'location_id' => 'numeric|exists:locations,id|min:1',
            'lecturer_id' => 'numeric|exists:users,id|min:1',
            'capacity' => 'numeric|min:1',
            'course_id' => 'numeric|exists:courses,id|min:1',
            'shift_id' => 'numeric|exists:shifts,id|min:1'
        ]);
        
        $status = 200;
        $message = "OK";
        $error = "";
        if($validator->fails()){
            $status = 400;
            $message =   "error";
            $error = $validator->errors();
        } else {
            $INCLUDE_FIELDS = ['period','term_id','location_id','lecturer_id'
            ,'capacity','course_id'];
            $class->update($request->only($INCLUDE_FIELDS));
            
            $class->master_day = $request->day == null ? $class->master_day : $request->day;
            $class->master_shift_id = $request->shift_id == null ? $class->master_shift_id :$request->shift_id ;
            $class->save();
            $class->class_schedule = $this->classScheduleController->generateUpdatesClassSchedules($class);
            if(!$class){
                $message = "Failed Update Data";
                $status = 400;
            }
        }
        
        return response()->json([
            "status" => $status,
            "message" => $message,
            "error" => $error
        ]);
    }

    
    public function destroy(UserClass $class)
    {
        $status = 200;
        $message = "OK";
        if(!$class){
            $status = 400;
            $message = "No Data Found";
        }
        
        $class->delete();
        return response()->json([
            "status" => $status,
            "message" => $message
        ]);
    }

    public function getScoreByClass($classId){
        $scores = UserClass::with(['term'=>function($query){
            $query->select('id','term_name');
        },
        'course'=>function($query){
            $query->select('id','course_name');
        },
        'location'=>function($query){
            $query->select('id','location_name');
        },
        'user'=>function($query){
            $query->select('id','name');
        },
        'shift'=>function($query){
            $query->select('id','begin_time','end_time');
        }
        ])->find($classId)->makeHidden(['created_at','updated_at','term_id','course_id','location_id','lecturer_id','master_shift_id']);
        $scores->classDetail->map(function($item,$key){
            $USER_HIDDEN_DATA = ["user_id","email","gender","email_verified_at","date_of_birth","major_id","phone_number","linkedin","instagram_id","line_id","role_id","created_at","updated_at"];
            $CLASS_DETAIL_HIDDEN_DATA = ['created_at','updated_at','member_id','user_class_id'];
            $SCORES_HIDDEN_DATA = ["class_detail_id","created_at","updated_at"];

            $item->makeHidden($CLASS_DETAIL_HIDDEN_DATA);
            if($item->score !=null){
                $item->score->makeHidden($SCORES_HIDDEN_DATA);
            }
            $item->user->makeHidden($USER_HIDDEN_DATA);
        });

        return response()->json([
            "status" => 200,
            "message" => $scores
        ]);
    }

    //refactor area

    private function getClassWhere($conditon){
        return $classes = DB::table('user_classes')
        ->join('terms','user_classes.term_id','=','terms.id')
        ->join('courses','user_classes.course_id','=','courses.id')
        ->join('locations','user_classes.location_id','=','locations.id')
        ->join('users','user_classes.lecturer_id','=','users.id')
        ->join('shifts','user_classes.master_shift_id','=','shifts.id')
        ->leftJoin('class_details', 'user_classes.id','=','class_details.user_class_id')
        ->select('user_classes.id','user_classes.period','user_classes.term_id',
        'user_classes.course_id','user_classes.lecturer_id','user_classes.location_id',
        'user_classes.master_day as day','user_classes.master_shift_id as shift_id','user_classes.capacity',
        'terms.term_name','courses.course_name','locations.location_name',
        'users.name as lecturer_name','users.email as lecturer_email','users.phone_number as lecturer_phone_number',
        'shifts.begin_time as class_begin_time','shifts.end_time as class_end_time')
        ->where($conditon)
        ->distinct()
        ->get();
    }

    private function getMemberInClass($classes){
        foreach($classes as $class){
            $class->members = DB::table('class_details')
            ->join('users','users.id','=','class_details.member_id')
            ->where('class_details.user_class_id','=',$class->id)
            ->select('users.id as user_id','users.user_id as member_id','users.name as member_name')
            ->get();   
        }
        return $classes;
    }

    private function countRegisteredMember($classes){
        foreach($classes as $class){
            $registerMember = DB::table('class_details')->where(['user_class_id'=>$class->id])->count();
            $class->register_member = $registerMember;
        }
        return $classes;
    }
}