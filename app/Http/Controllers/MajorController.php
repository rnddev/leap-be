<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Carbon\carbon;
use App\Major;

class MajorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $major = Major::All();
        $status = 200;
        $message = "ok";
        if($major == null){
            $status = 502;
            $message = "Data Not Found";
        }
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $major
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $status = 201;
        $message = "Successfully Created Major";
        
        $validator = Validator::make($request->all(), [
            'major_name' => 'required|unique:majors|max:255|regex:/^([a-zA-Z]+\s)*[a-zA-Z]+$/',
        ]);
        
        if($validator->fails()){
            $status = 400;
            $message = $validator->errors()->first();  
            $major = null;
        } else {
            $major = new Major;
            $major->major_name = $request->major_name;
            $major->created_at = Carbon::now();
            $major->save();

            if(!$major){
                $status = 400;
                $message = "Failed Created Major";    
            }
        }

        return response()->json([
            "status" => $status,
            "message" => $message,
            'data' => $major
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Major $major)
    {
        $status = 200;
        $message = "ok";
        if(!$major){
            $status = 502;
            $message = "Data Not Found";
        }
        return response()->json([
            "status" => $status,
            "message" => $message,
            "data" => $major
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Major $major)
    {
        $major->major_name = $request->major_name;
        $major->save();
        $message = "Succefully Update Data";
        $status = 200;
        
        if(!$major){
            $message = "Failed Update Data";
            $status = 400;
        }

        return response()->json([
            "status" => $status,
            "message" => $message,
            "data" => $major
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Major $major)
    {
        $status = 200;
        $message = "Deleted Data";
        if(!$major){
            $status = 400;
            $message = "No Data Found";
        }
        $major->delete();

        return response()->json([
            "status" => $status,
            "message" => $message
        ]);
    }
}
