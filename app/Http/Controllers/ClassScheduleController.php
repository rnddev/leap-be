<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\ClassSchedule;
use App\AcademicCalendar;
use App\Curriculum;
use App\ClassDetail;
use App\UserClass;
use App\Course;
use App\Location;
use Auth;

class ClassScheduleController extends Controller
{

    public function generateClassSchedule(UserClass $request){
        $meeting_dates = $this->getMeetingDates($request);
        $class_count = $this->insertClassSchedule($request, $meeting_dates);
        $inserted_dates = array_slice($meeting_dates, 0, $class_count);

        return $inserted_dates;
    }

    public function createClassSchedule(Request $request) {
        $validator = Validator::make(request()->all(),[
            'id' => 'required|numeric',
            'period' => 'required|numeric',
            'term_id' => 'required|numeric',
            'course_id' => 'required|numeric',
            'master_day' => 'required|string',
            'master_shift_id' => 'required|numeric',
        ]);

        if($validator->fails()) {
            return response()->json($validator->messages(),400);
        }

        $meeting_dates = $this->getMeetingDates($request);

        $class_count = $this->insertClassSchedule($request, $meeting_dates);

        $inserted_dates = array_slice($meeting_dates, 0, $class_count);

        return response()->json([
            'status' => 201,
            'message' => 'OK',
            'error' => "",
            'inserted_dates' => $inserted_dates
        ]);
    }

    public function getAllClassSchedule($user_class_id) {
        $schedules = ClassSchedule::where('user_class_id',$user_class_id)->get();

        // foreach ($schedules as $schedule) {
        //     $date = $this->getCarbonObject($schedule->meeting_date);
        //     $day_and_date = $date->format('l, d F Y');
        //     $schedule->day_and_date = $day_and_date;
        // }

        return response()->json([
            $schedules,
        ]);
    }

    public function getNextClasses ($user_id) {
        $user_class_id = $this->getUserClassId($user_id);

        $today = $this->getTodayDate();

        $next_classes = ClassSchedule::where('user_class_id', $user_class_id)->whereDate('meeting_date', '>=', $today)->get();

        return response()->json([
            'status' => 200,
            'message' => 'OK',
            'error' => "",
            'next_classes' => $next_classes
        ]);
    }

    public function getUserClassCount() {
        $user_class_id = $this->getUserClassId(auth()->user()->id);

        $class_schedules = ClassSchedule::where('user_class_id', $user_class_id)->get();

        $count = $class_schedules->count();

        return response()->json([
            'status' => 200,
            'message' => 'OK',
            'error' => "",
            'class_count' => $count
        ]);
    }

    public function getPreviousClassesWithCurriculumAndClass() {
        // dd(auth()->user()->id);
        $user_class_id = $this->getUserClassId(auth()->user()->id);

        $today = $this->getTodayDate();

        $prev_classes = ClassSchedule::where('user_class_id', $user_class_id)->whereDate('meeting_date', '<', $today)
                        ->join('curricula', 'class_schedules.curriculum_id', '=', 'curricula.id')
                        ->join('user_classes', 'class_schedules.user_class_id', '=', 'user_classes.id')
                        ->join('shifts', 'class_schedules.shift_id', '=', 'shifts.id')
                        ->join('users', 'user_classes.lecturer_id', '=', 'users.id')
                        ->select('class_schedules.*', 'curricula.meeting_name', 'curricula.material', 'shifts.begin_time', 'shifts.end_time', 'users.name as lecturer_name')
                        ->get();

        return response()->json([
            'status' => 200,
            'message' => 'OK',
            'error' => "",
            'prev_classes' => $prev_classes
        ]);
    }

    public function getNextClassesWithCurriculumAndClass() {
        // dd(auth()->user()->id);
        $user_class_id = $this->getUserClassId(auth()->user()->id);

        $course_name = $this->getUserCourseName($user_class_id);
        
        $location = $this->getUserClassLocation($user_class_id);

        $today = $this->getTodayDate();

        $next_classes = ClassSchedule::where('user_class_id', $user_class_id)->whereDate('meeting_date', '>=', $today)
                        ->join('curricula', 'class_schedules.curriculum_id', '=', 'curricula.id')
                        ->join('user_classes', 'class_schedules.user_class_id', '=', 'user_classes.id')
                        ->join('shifts', 'class_schedules.shift_id', '=', 'shifts.id')
                        ->join('users', 'user_classes.lecturer_id', '=', 'users.id')
                        ->select('class_schedules.*', 'curricula.meeting_name', 'curricula.material', 'shifts.begin_time', 'shifts.end_time', 'users.name as lecturer_name')
                        ->get();

        return response()->json([
            'status' => 200,
            'message' => 'OK',
            'error' => "",
            'next_classes' => $next_classes,
            'course_name' => $course_name,
            'location' => $location
        ]);
    }

    public function getLecturerNextClassesWithCurriculumAndClass() {
        $today = $this->getTodayDate();

        $academic_calendar = AcademicCalendar::whereDate('begin_date', '<=', $today)->whereDate('end_date', '>=', $today)->first();

        $lecturer_id = auth()->user()->id;
        $lecturer_classes = UserClass::where('lecturer_id', $lecturer_id)
        ->where('period', $academic_calendar->period)
        ->where('term_id', $academic_calendar->term_id)
        ->get();

        

        $test = array();

        foreach($lecturer_classes as $key => $lc){
            $lc->location = $location = $this->getUserClassLocation($lc->id);
            $course_name = $this->getUserCourseName($lc->id);
            $lc->course_name = $course_name;
            $lc->next_classes = ClassSchedule::where('user_class_id', $lc->id)->whereDate('meeting_date', '>=', $today)
            ->join('curricula', 'class_schedules.curriculum_id', '=', 'curricula.id')
            ->join('shifts', 'class_schedules.shift_id', '=', 'shifts.id')
            ->select('class_schedules.*', 'curricula.meeting_name', 'shifts.begin_time', 'shifts.end_time')
            ->get();

            $d = Carbon::createFromFormat('Y-m-d H:i:s', $lc->next_classes[0]->meeting_date." ".$lc->next_classes[0]->begin_time);
            array_push($test , $d);

            
        }


        $lecturer_classes = $lecturer_classes->sortBy(function($transaction)
        {
            return $transaction->next_classes[0]->begin_time;
        })->sortBy(function($transaction)
        {
            return $transaction->next_classes[0]->meeting_date;
        })->values(); // tanggal paling duluan dengan waktu paling duluan
        return response()->json([
            'status' => 200,
            'message' => 'OK',
            'error' => "",
            'lecturer_classes' => $lecturer_classes,
        ]);
    }

    public function getAllCurrentClassesMaterial() {
        $user_class_id = $this->getUserClassId(auth()->user()->id);

        $today = $this->getTodayDate();

        $prev_classes = ClassSchedule::where('user_class_id', $user_class_id)->whereDate('meeting_date', '<=', $today)
                        ->join('curricula', 'class_schedules.curriculum_id', '=', 'curricula.id')
                        ->select('class_schedules.*', 'curricula.meeting_name', 'curricula.material')
                        ->get();

        $next_classes = ClassSchedule::where('user_class_id', $user_class_id)->whereDate('meeting_date', '>=', $today)
                        ->join('curricula', 'class_schedules.curriculum_id', '=', 'curricula.id')
                        ->select('class_schedules.*', 'curricula.meeting_name', 'curricula.material')
                        ->get();

        return response()->json([
            'status' => 200,
            'message' => 'OK',
            'error' => "",
            'prev_classes' => $prev_classes,
            'next_classes' => $next_classes
        ]);
    }



    public function generateUpdatesClassSchedules(UserClass $request){
        $meeting_dates = $this->getMeetingDates($request);

        $class_count = $this->updateClassSchedule($request, $meeting_dates);

        $updated_dates = array_slice($meeting_dates, 0, $class_count);

        return $updated_dates;
    }

    public function updateAllClassSchedules(Request $request) {
        $validator = Validator::make(request()->all(),[
            'id' => 'required|numeric',
            'period' => 'required|numeric',
            'term_id' => 'required|numeric',
            'course_id' => 'required|numeric',
            'master_day' => 'required|string',
            'master_shift_id' => 'required|numeric',
        ]);

        if($validator->fails()) {
            return response()->json($validator->messages(),400);
        }

        $meeting_dates = $this->getMeetingDates($request);

        $class_count = $this->updateClassSchedule($request, $meeting_dates);

        $updated_dates = array_slice($meeting_dates, 0, $class_count);

        return response()->json([
            'status' => 201,
            'message' => 'OK',
            'error' => "",
            'inserted_dates' => $updated_dates
        ]);
    }

    public function updateClassScheduleTime(Request $request, $class_schedule_id) {
        $validator = Validator::make(request()->all(),[
            'meeting_date' => 'required|date',
            'shift_id' => 'required|numeric'
        ]);

        if($validator->fails()) {
            return response()->json($validator->messages(),400);
        }

        $this->editClassScheduleTime($request, $class_schedule_id);

        return response()->json([
            'status' => 201,
            'message' => 'OK',
            'error' => ""
        ]);
    }

    private function getMeetingDates($request) {
        $HOLIDAY_ID = 1;
        $ROUTINE_ID = 2;
        
        $routine_calendars = $this->getAcademicCalendars($request, $ROUTINE_ID);
        $holiday_calendars = $this->getAcademicCalendars($request, $HOLIDAY_ID);
        $meeting_dates = $this->getClassDates($request, $routine_calendars, $holiday_calendars);

        return $meeting_dates;
    }

    private function getAcademicCalendars($request, $status_id) {
        $period = $request->period;
        $term = $request->term_id;
        
        $calendars = AcademicCalendar::where('period', $period)->where('calendar_status_id', $status_id)
                    ->where('term_id', $term)->orderBy('begin_date', 'ASC')->get();

        return $calendars;
    }

    private function getClassDates($request, $routine_calendars, $holiday_calendars) {
        $meeting_day = $request->master_day;
        
        $routines = $this->getCalendarDates($routine_calendars, $meeting_day);
        $holidays = $this->getCalendarDates($holiday_calendars, $meeting_day);
        
        $diff = array_diff($routines, $holidays);

        return $diff;
    }

    private function getCalendarDates($calendars, $meeting_day) {
        $dates = [];

        foreach($calendars as $calendar) {
            $meeting_date = $this->getFirstMeetingDate($calendar, $meeting_day);
            $begin_date = $this->getCarbonObject($calendar->begin_date);
            $end_date = $this->getCarbonObject($calendar->end_date);
            
            while ($meeting_date->between($begin_date, $end_date)) {
                $mt_f = $meeting_date->format('Y-m-d');
                if (!in_array($mt_f, $dates)) {
                    array_push($dates, $mt_f);
                }
                $meeting_date = $meeting_date->addWeek();
            }
        }

        return $dates;
    }

    private function insertClassSchedule($request, $meeting_dates) {
        $count = 1;
        $shift_id = $request->master_shift_id;
        $user_class_id = $request->id;
        $day = $request->master_day;

        foreach ($meeting_dates as $date) {
            $schedule = new ClassSchedule();
            $schedule->shift_id = $shift_id;
            $schedule->curriculum_id = $this->getCurriculumId($request, $count); //this should be on curriculum model
            if (!$schedule->curriculum_id) {
                break;
            }
            $schedule->user_class_id = $user_class_id;
            $schedule->meeting_date = $date;
            $schedule->day = $day;
            $schedule->save();
            $count++;
        }

        return $count-1;
    }

    private function getCurriculumId($request, $count) {
        $course_id = $request->course_id;
        $term_id = $request->term_id;
        $curriculum = Curriculum::where('course_id', $course_id)->where('term_id', $term_id)->where('meeting',$count)->first();
        
        if (!$curriculum) {
            return NULL;
        } else {
            return $curriculum->id;
        }
    }

    //code should be on class detail model
    private function getUserClassId($user_id) {
        $class_detail_id = ClassDetail::where('member_id', $user_id)->first();
        $user_class_id = $class_detail_id->user_class_id;

        return $user_class_id;
    }

    private function updateClassSchedule($request, $meeting_dates) {
        $count = 1;
        $shift_id = $request->master_shift_id;
        $user_class_id = $request->id;
        $day = $request->master_day;

        $schedule = ClassSchedule::where('user_class_id', $user_class_id)->delete();

        foreach ($meeting_dates as $date) {
            $schedule = new ClassSchedule();
            $schedule->shift_id = $shift_id;
            $schedule->curriculum_id = $this->getCurriculumId($request, $count); //this should be on curriculum model
            if ($schedule->curriculum_id == NULL) {
                break;
            }
            $schedule->user_class_id = $user_class_id;
            $schedule->meeting_date = $date;
            $schedule->day = $day;
            $schedule->save();
            $count++;
        }

        return $count-1;
    }

    private function editClassScheduleTime($request, $class_schedule_id) {
        $date = $this->getCarbonObject($request->meeting_date);
        $shift_id = $request->shift_id;

        $schedule = ClassSchedule::find($class_schedule_id);
        $schedule->meeting_date = $date->format('Y-m-d');
        $schedule->day = $date->format('l');
        $schedule->shift_id = $shift_id;
        $schedule->save();
    }

    //This code should be on a service controller by OOP Principles
    private function getFirstMeetingDate($calendar, $day) {
        $flag = 0;

        $cday = $this->getCarbonObject($calendar->begin_date);
            while ($day != $cday->format('l')) {
                $cday->addDays(1);
        }

        return $cday;
    }

    private function getUserCourseName($user_class_id) {
        $user_class = UserClass::find($user_class_id);
        $course_id = $user_class->course_id;

        $course = Course::find($course_id);

        return $course->course_name;
    }

    private function getUserClassLocation($user_class_id) {
        $user_class = UserClass::find($user_class_id);
        $location_id = $user_class->location_id;

        $location = Location::find($location_id);

        return $location->location_name;
    }

    private function getTodayDate() {
        $today = Carbon::now()->setTimezone('Asia/Jakarta')->toDateString();
        return $today;
    }

    private function getCarbonObject($date) {
        return Carbon::parse($date);
    }

}
