<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Assignment;
use App\SubmittedAssignment;
use Carbon\Carbon; 
use App\ClassDetail;
use App\UserClass;
use Storage;
use Validator;
use Auth;

class AssignmentController extends Controller
{
    public function getAllAssignment(){
      $assignment = Assignment::All();

      return response()->json([
        'status'=> 200,
        'message' => 'OK',
        'data' => $assignment->makeHidden(['created_at','updated_at'])
      ]);
    }

    public function updateAssignment(Request $request, $id){
      $assignment =  Assignment::where('course_id', $id)->firstOrFail();
      $validator = Validator::make(request()->all(),[
        'mid_exam_name' => 'string|between:1,250',
        'mid_requirement'=> 'string|between:1,250',
        'mid_judgment_criteria'=> 'string|between:1,250',
        'mid_exam_file' => 'mimes:pdf,doc,docx,ppt,pptx,xls,xlsx,odt,jpeg,jpg,png',
        'final_exam_name' => 'string|between:1,250',
        'final_requirement'=> "string|between:1,250",
        'final_judgment_criteria'=> 'string|between:1,250',
        'final_exam_file' => 'mimes:pdf,doc,docx,ppt,pptx,xls,xlsx,odt,jpeg,jpg,png',
        'course_id' => 'integer|min:1|max:5|exists:courses,id',
        ]);
        
        
        if($validator->fails()){
          return response()->json($validator->messages(),400);
        }
        $assignment->update($request->only($assignment->getFillable()));
   
      $check = FALSE;
      if($request->hasFile('mid_exam_file')) {
          Storage::disk('public')->delete($assignment->mid_exam_file);

          $check = TRUE;

          $mid_exam_file_name = $assignment->course_id.
          '_'.$request->mid_exam_name.
          '_'.Carbon::now()->timestamp.'.'.$request->mid_exam_file->extension();

          $assignment->mid_exam_file = $mid_exam_file_name;

          Storage::disk('public')->put($mid_exam_file_name, file_get_contents($request->mid_exam_file));
      }

      if(!$check){
          $assignment->mid_exam_file = $assignment->mid_exam_file;
      }
    

      $check = FALSE;
      if($request->hasFile('final_exam_file')) {
          Storage::disk('public')->delete($assignment->final_exam_file);

          $check = TRUE;

          $final_exam_file_name = $assignment->course_id.
          '_'.$request->final_exam_name.
          '_'.Carbon::now()->timestamp.'.'.$request->final_exam_file->extension();
          $assignment->final_exam_file = $final_exam_file_name;

          Storage::disk('public')->put($final_exam_file_name, file_get_contents($request->final_exam_file));
      }

      if(!$check){
          $assignment->final_exam_file = $assignment->final_exam_file;
      }
      
      $assignment->save();


      return response()->json([
          'status' => 200,
          'message' => 'OK',
          'error' => ""
      ]);
    }


public function storeAssignment(Request $request) {
    $validator = Validator::make(request()->all(),[
      'mid_exam_name' => 'required|string|between:1,250',
      'mid_requirement'=> 'required|string|between:1,250',
      'mid_judgment_criteria'=> 'required|string|between:1,250',
      'mid_exam_file' => 'required|mimes:pdf,doc,docx,ppt,pptx,xls,xlsx,odt,jpeg,jpg,png',
      'final_exam_name' => 'required|string|between:1,250',
      'final_requirement'=> "required|string|between:1,250",
      'final_judgment_criteria'=> 'required|string|between:1,250',
      'final_exam_file' => 'required|mimes:pdf,doc,docx,ppt,pptx,xls,xlsx,odt,jpeg,jpg,png',
      'course_id' => 'required|integer|min:1|max:5|exists:courses,id',
      ]);
        
        
    if($validator->fails()){
      return response()->json($validator->messages(),400);
    }
    // $assignment->update($request->only($assignment->getFillable()));
   
      $assignment =  new Assignment;

      $assignment->course_id = $request->course_id;

      $assignment->mid_exam_name = $request->mid_exam_name;
      $assignment->mid_requirement = $request->mid_requirement;
      $assignment->mid_judgment_criteria = $request->mid_judgment_criteria;

      $assignment->final_exam_name = $request->final_exam_name;
      $assignment->final_requirement = $request->final_requirement;
      $assignment->final_judgment_criteria = $request->final_judgment_criteria;

      $mid_exam_file_name = $assignment->course_id.
      '_'.$request->mid_exam_name.'_MID'.
      '_'.Carbon::now()->timestamp.'.'.$request->mid_exam_file->extension();

      $assignment->mid_exam_file = $mid_exam_file_name;

      Storage::disk('public')->put($mid_exam_file_name, file_get_contents($request->mid_exam_file));

      $final_exam_file_name = $assignment->course_id.
      '_'.$request->final_exam_name.'_FINAL'.
      '_'.Carbon::now()->timestamp.'.'.$request->final_exam_file->extension();
      $assignment->final_exam_file = $final_exam_file_name;

      Storage::disk('public')->put($final_exam_file_name, file_get_contents($request->final_exam_file));
      
      $assignment->save();

      return response()->json([
          'status' => 200,
          'message' => 'OK',
          'error' => ""
      ]);
  }

  public function getCurrentCourseAssignment() {
    $user_id = auth()->user()->id;

    $class_detail = ClassDetail::where('member_id', $user_id)->orderBy('id', 'desc')->first();

    $course = UserClass::where('id', $class_detail->id)->orderBy('id', 'desc')->first();

    $assignment = Assignment::where('course_id', $course->course_id)->orderBy('created_at', 'desc')->first();

    $submitted_assignment = SubmittedAssignment::where('class_detail_id', $class_detail->id)->first();

    return response()->json([
      'status' => 200,
      'message' => "OK",
      'data' => $assignment,
      'class_detail_id' => $class_detail->id,
      'submitted_assignment' => $submitted_assignment
    ]);
  }
}


