<?php

namespace App\Http\Controllers;
use Storage;
use Carbon\Carbon; 
use Validator;
use App\SubmittedAssignment;
use Illuminate\Http\Request;
use App\ClassDetail;

class SubmitAssignmentController extends Controller
{
    public function getAllSubmittedAssignment()
    {
        $submittedAssignments = SubmittedAssignment::all();
        $status = 200;
        $message = "ok";
        if (sizeof($submittedAssignments) == 0) {
            $status = 502;
            $message = "Data not Found";
        }
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $submittedAssignments
        ]);
    }

    public function show(SubmittedAssignment $assignment)
    {
        $status = 200;
        $message = "ok";
        $submittedAssignment = SubmittedAssignment::where('id', $assignment->id)->get();
        if (sizeof($submittedAssignment) == 0) {
            $status = 502;
            $message = "Data not Found";
        }
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $submittedAssignment
        ]);
    }

    public function update(Request $request, $classDetailId)
    {
        $assignment = SubmittedAssignment::where('class_detail_id',$classDetailId)->get()->first();

        $validator = Validator::make($request->all(), [
            'mid_exam_file' => 'mimes:pdf,doc,docx,ppt,pptx,xls,xlsx,odt,jpeg,jpg,png',
            'final_exam_file' => 'mimes:pdf,doc,docx,ppt,pptx,xls,xlsx,odt,jpeg,jpg,png',
        ]);
        $status = 200;
        $message = "Successfully Update Submitted Assignment";
        $error = "";
        if($validator->fails()){
            return response()->json($validator->messages(),400);
        }
     
        if($request->hasFile('mid_exam_file')) {
            Storage::disk('public')->delete($assignment->mid_exam_file);
  
            $mid_exam_file_name = $assignment->class_detail_id.
            '_'.$request->mid_exam_name.
            '_'.Carbon::now()->timestamp.'.'.$request->mid_exam_file->extension();
  
            $assignment->mid_exam_file = $mid_exam_file_name;
  
            Storage::disk('public')->put($mid_exam_file_name, file_get_contents($request->mid_exam_file));
        }

        if($request->hasFile('final_exam_file')) {
            Storage::disk('public')->delete($assignment->final_exam_file);
  
            $final_exam_file_name = $assignment->class_detail_id.
            '_'.$request->final_exam_name.
            '_'.Carbon::now()->timestamp.'.'.$request->final_exam_file->extension();
            $assignment->final_exam_file = $final_exam_file_name;
  
            Storage::disk('public')->put($final_exam_file_name, file_get_contents($request->final_exam_file));
        }
        $assignment->save();
        return response()->json([
            "status" => $status,
            "message" => $message,
            "error" => $error
        ]);
    }

    public function storeMidExam(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mid_exam_file' => 'required|mimes:zip,rar,pdf,doc,docx,ppt,pptx,xls,xlsx,odt,jpeg,jpg,png',
        ]);
        if($validator->fails()){
            return response()->json($validator->messages(),400);
        }

        $user_id = auth()->user()->id;

        $class_detail = ClassDetail::where('member_id', $user_id)->orderBy('id', 'desc')->first();

        $classDetailId = $class_detail->id;

        $assignment = null;
        if(SubmittedAssignment::where('class_detail_id',$classDetailId)->get()->first() == null) {
            $assignment = new SubmittedAssignment;
        } else {
            $assignment = SubmittedAssignment::where('class_detail_id',$classDetailId)->get()->first();
        }

        $assignment->class_detail_id = $classDetailId;

        if($assignment->mid_exam_file == null) {
            $mid_exam_file_name =  'mid_project'.
            '/'.$assignment->class_detail_id.
            '_'.auth()->user()->email.
            '_'.Carbon::now()->timestamp.'.'.$request->mid_exam_file->extension();
  
            $assignment->mid_exam_file = $mid_exam_file_name;
  
            Storage::disk('public')->put($mid_exam_file_name, file_get_contents($request->mid_exam_file));
        } else {
            Storage::disk('public')->delete($assignment->mid_exam_file);
  
            $mid_exam_file_name = 'mid_project'.
            '/'.$assignment->class_detail_id.
            '_'.auth()->user()->email.
            '_'.Carbon::now()->timestamp.'.'.$request->mid_exam_file->extension();
  
            $assignment->mid_exam_file = $mid_exam_file_name;
  
            Storage::disk('public')->put($mid_exam_file_name, file_get_contents($request->mid_exam_file));
        }
        $assignment->save();
        return response()->json([
            'status' => 201,
            'message' => 'OK',
            'error' => ""
        ]);
    }

    public function storeFinalExam(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'final_exam_file' => 'required|mimes:zip,rar,pdf,doc,docx,ppt,pptx,xls,xlsx,odt,jpeg,jpg,png',
        ]);

        if($validator->fails()){
            return response()->json($validator->messages(),400);
        }

        $user_id = auth()->user()->id;

        $class_detail = ClassDetail::where('member_id', $user_id)->orderBy('id', 'desc')->first();

        $classDetailId = $class_detail->id;

        $assignment = null;
        if(SubmittedAssignment::where('class_detail_id',$classDetailId)->get()->first() == null) {
            $assignment = new SubmittedAssignment;
        } else {
            $assignment = SubmittedAssignment::where('class_detail_id',$classDetailId)->get()->first();
        }

        $assignment->class_detail_id = $classDetailId;

        if($assignment->final_exam_file == null) {
            $final_exam_file_name = 'final_project'.
            '/'.$assignment->class_detail_id.
            '_'.auth()->user()->email.
            '_'.Carbon::now()->timestamp.'.'.$request->final_exam_file->extension();
            $assignment->final_exam_file = $final_exam_file_name;
  
            Storage::disk('public')->put($final_exam_file_name, file_get_contents($request->final_exam_file));
        } else {
            Storage::disk('public')->delete($assignment->final_exam_file);
  
            $final_exam_file_name = 'final_project'.
            '/'.$assignment->class_detail_id.
            '_'.auth()->user()->email.
            '_'.Carbon::now()->timestamp.'.'.$request->final_exam_file->extension();
            $assignment->final_exam_file = $final_exam_file_name;
  
            Storage::disk('public')->put($final_exam_file_name, file_get_contents($request->final_exam_file));
        }

        $assignment->save();
        return response()->json([
            'status' => 201,
            'message' => 'OK',
            'error' => ""
        ]);
    }
}
