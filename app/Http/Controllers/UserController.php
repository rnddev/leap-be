<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;

class UserController extends Controller
{
    
    public function createMember(Request $request){
        $response = $this->createUser($request,3);
        return response()->json($response);
    }
    
    public function createLecturer(Request $request){
        $response = $this->createUser($request,2);
        return response()->json($response);
    }
    
    public function createAdmin(Request $request){
        $response = $this->createUser($request,1);
        return response()->json($response);
    }
    
    public function getAllMember(){
        $users = User::where('role_id',3)->get();
        return response()->json(
            [
                "status" => 200,
                "message" => "Success",
                "data" => $users
            ]
        );
    }
    
    public function getAllLecturer(){
        $users = User::where('role_id',2)->get();
        return response()->json(
            [
                "status" => 200,
                "message" => "Success",
                "data" => $users
            ]
        );
    }

    public function getMember(User $member){
        if(!$member){
            $status = 400;
            $message = "Failed to show User";
        }else{
            $status = 200;
            $message = "Success to show User";
        }

        if(!$this->isMember($member)){
            $status = 400;
            $message = "User with this id is not a member";
            $member = null;
        }

        return response()->json([
            'status'=> $status,
            'message'=> $message,
            'data' => $member
        ]);
    }

    public function getLecturer(User $lecturer){
        if(!$lecturer){
            $status = 400;
            $message = "Failed to show User";
        }else{
            $status = 200;
            $message = "Success to show User";
        }
        if(!$this->isLecturer($lecturer)){
            $status = 400;
            $message = "User with this id is not a lecturer";
            $lecturer = null;
        }

        return response()->json([
            'status'=> $status,
            'message'=> $message,
            'data' => $lecturer
        ]);
    }


    public function updateMember(Request $request, User $member){
        if($this->isMember($member)){
            return response()->json($this->updateUser($request,$member));
        }
        return response()->json([
            'status' => 400,
            'message' => 'user is not a member'
        ]);
    }

    public function updateLecturer(Request $request, User $lecturer){
        if($this->isLecturer($lecturer)){
            return response()->json($this->updateUser($request,$lecturer));
        }
        return response()->json([
            'status' => 400,
            'message' => 'user is not a lecturer'
        ]);
    }
    
    public function deleteMember(User $member){
        if($this->isMember($member)){
            return response()->json($this->deleteUser($member));
        }
        return response()->json([
            'status' => 400,
            'message' => 'user is not a member'
        ]);
    }

    public function deleteLecturer(User $lecturer){
        if($this->isLecturer($lecturer)){
            return response()->json($this->deleteUser($lecturer));
        }
        return response()->json([
            'status' => 400,
            'message' => 'user is not a lecturer'
        ]);
    }

    private function updateUser(Request $request, User $user){
        $validator = Validator::make($request->all(),[
            'user_id' => 'unique:users',
            'email' => 'unique:users',
            'phone_number' => 'unique:users',
            'major_id' => 'exists:majors,id'
        ]);
        
        if(!$validator->fails()){
            $INCLUDE_FIELDS = ['user_id','name','email','date_of_birth'
            ,'gender','phone_number','instagram_id','linkedin','line_id','major_id'];
            
            $user->update($request->only($INCLUDE_FIELDS));
            
            if(!$user){
                $message  = "Failed to updated user";
                $status = 400;
            }else{
                $message  = "Successfully updated user";
                $status = 200;
            }
            $user->save();
        } else {
            $message  = $validator->errors();
            $status = 400;
            return [
                'status' => $status,
                'message' => $message,
            ];
        }
        return [
            'status' => $status,
            'message' => $message,
            'data' => $user
        ];
    }


    private function isMember($user){
        return $user->role_id == 3;
    }
    private function isLecturer($user){
        return $user->role_id == 2;
    }
    private function deleteUser(User $user){
        if(!$user){
            $message  = "Failed to delete User";
            $status = 400;
            
        }else{
            $message  = "Successfully delete User";
            $status = 200;
        }
        $user->delete();
        
        return [
            'status' => $status,
            'message' => $message
        ];
    }

    private function createUserToDB(Request $request, $role_id){
        $user = new User;
        $user->user_id = $request->user_id;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->date_of_birth = $request->date_of_birth;
        $user->gender = $request->gender;
        $user->phone_number = $request->phone_number;
        $user->instagram_id = $request->instagram_id;
        $user->linkedin = $request->linkedin;
        $user->line_id = $request->line_id;
        $user->major_id = $request->major_id;
        $user->role_id = $role_id;
        $user->created_at = Carbon::now();
        $user->save();
        return $user;
    }

    private function validateCreateuser(Request $request){
        $validator = Validator::make($request->all(),[
            'user_id' => 'required|unique:users',
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',
            'date_of_birth' => 'required',
            'gender' => 'required',
            'linkedin' =>'required',
            'phone_number' => 'required|unique:users',
            'instagram_id' => 'required',
            'line_id' => 'required',
            'major_id' => 'required|exists:majors,id'
        ]);
        return $validator;
    }
    
    private function createUser(Request $request,$role_id){
        $validator = $this->validateCreateuser($request);
        if(!$validator->fails()){
            $user = $this->createUserToDB($request,$role_id);
            if(!$user){
                $message  = "Failed to create new User!";
                $status = 400;
            }else{
                $message  = "Successfully create new User!";
                $status = 200;
            }
             return ['status' => $status,'message' => $message,'data' => $user];
        } else {
            $message  = $validator->errors();
            $status = 400;
            return ['status' => $status,'message' => $message];
        }
    }
}
