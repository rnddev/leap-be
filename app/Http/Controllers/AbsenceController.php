<?php

namespace App\Http\Controllers;

use App\Absence;
use App\ClassDetail;
use App\ClassSchedule;
use App\AcademicCalendar;
use App\UserClass;
use App\Course;
use App\Location;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use Validator;

class AbsenceController extends Controller
{
    public function storeUserAbsence(Request $request) {
        $validator = Validator::make(request()->all(),[
            // 'user_class_id' => 'required|numeric',
            'attend_member_id' => 'array|nullable',
            'absent_member_id' => 'array|nullable',
            'class_schedule_id' => 'required|numeric',
        ]);

        if($validator->fails()) {
            return response()->json($validator->messages(),400);
        }

        $class_schedule_id = $request->class_schedule_id;
        
        foreach($request->attend_member_id as $member) {
            $class_detail_id = $this->getClassDetailId($member);

            $absence = new Absence();
            $absence->absence = 1;
            $absence->class_detail_id = $class_detail_id;
            $absence->class_schedule_id = $class_schedule_id;
            $absence->save();
        }

        foreach($request->absent_member_id as $member) {
            $class_detail_id = $this->getClassDetailId($member);

            $absence = new Absence();
            $absence->absence = 0;
            $absence->class_detail_id = $class_detail_id;
            $absence->class_schedule_id = $class_schedule_id;
            $absence->save();
        }

        return response()->json([
            "status" => 201,
            "message" => "Attendance Successfully added",
            "error" => "Error submit attendance"
        ]);
    }

    public function update(Request $request)
    {   
        foreach($request->all() as $req){
            $absence = Absence::where(['class_detail_id'=>$req['class_detail_id'],'class_schedule_id'=>$req['class_schedule_id']])->get()->first();
            if($req['absence']!=null){
                $absence->absence = $req['absence'];
            }
            $absence->save();
            $message = "Absence Updated";
            $status = 200;
        }

        if(!$absence){
            $message = "Failed to Update Absence";
            $status = 400;
        }

        return response()->json([
            "status" => $status,
            "message" => $message,
            "error" => "" 
        ]);
    }

    public function getAttendanceStatus(Request $request) {
        $validator = Validator::make(request()->all(),[
            'class_schedule_id' => 'required|numeric'
        ]);

        if($validator->fails()) {
            return response()->json($validator->messages(),400);
        }

        $class_schedule_id = $request->class_schedule_id;
        
        $isSubmit = Absence::where('class_schedule_id', $class_schedule_id)->exists();

        return response()->json([
            "status" => 200,
            "message" => "OK",
            "error" => "Error",
            "submitted" => $isSubmit
        ]);
    }

    public function getUserAbsence() {
        $class_detail_id = $this->getClassDetailId(auth()->user()->id);

        $absence = Absence::where('class_detail_id', $class_detail_id)->get();

        return response()->json([
            "status" => 200,
            "message" => "OK",
            "error" => "",
            "absence" => $absence
        ]);
    }

    public function getUserAbsenceCount() {
        $class_detail_id = $this->getClassDetailId(auth()->user()->id);

        $absence = Absence::where('class_detail_id', $class_detail_id)->get();
        $present_count = $absence->where('absence', 1)->count();
        $absence_count = $absence->count();


        return response()->json([
            "status" => 200,
            "message" => "OK",
            "error" => "",
            "present" => $present_count,
            "total" => $absence_count
        ]);
    }

    public function getMemberData() {
        $user_class_id = $this->getLecturerNextClassId();
        $member_id = $this->getMemberId($user_class_id);

        $members = [];
        foreach ($member_id as $id) {
            $data = User::find($id);
            array_push($members, $data);
        }

        return response()->json([
            "status" => 200,
            "message" => "OK",
            "error" => "",
            "members" => $members,
            "class_id" => $user_class_id
        ]);
    }

    private function getMemberId($user_class_id) {
        $class_detail = ClassDetail::where('user_class_id', $user_class_id)->get();

        $member_id = [];
        foreach ($class_detail as $class) {
            array_push($member_id, $class->member_id);
        }

        return $member_id;
    }

    private function getClassDetailId($user_id) {
        $class_detail_id = ClassDetail::where('member_id', $user_id)->first();

        return $class_detail_id->id;
    }

    private function getLecturerNextClassId() {
        $today = $this->getTodayDate();

        $academic_calendar = AcademicCalendar::whereDate('begin_date', '<=', $today)->whereDate('end_date', '>=', $today)->first();
        $lecturer_id = auth()->user()->id;
        $lecturer_classes = UserClass::where('lecturer_id', $lecturer_id)
        ->where('period', $academic_calendar->period)
        ->where('term_id', $academic_calendar->term_id)
        ->get();

        

        $test = array();

        foreach($lecturer_classes as $key => $lc){
            $lc->location = $location = $this->getUserClassLocation($lc->id);
            $course_name = $this->getUserCourseName($lc->id);
            $lc->course_name = $course_name;
            $lc->next_classes = ClassSchedule::where('user_class_id', $lc->id)->whereDate('meeting_date', '>=', $today)
            ->join('curricula', 'class_schedules.curriculum_id', '=', 'curricula.id')
            ->join('shifts', 'class_schedules.shift_id', '=', 'shifts.id')
            ->select('class_schedules.*', 'curricula.meeting_name', 'shifts.begin_time', 'shifts.end_time')
            ->get();            
        }


        $lecturer_classes = $lecturer_classes->sortBy(function($transaction)
        {
            return $transaction->next_classes[0]->begin_time;
        })->sortBy(function($transaction)
        {
            return $transaction->next_classes[0]->meeting_date;
        })->values(); // tanggal paling duluan dengan waktu paling duluan
        
        return $lecturer_classes[0]->id;
    }

    private function getUserCourseName($user_class_id) {
        $user_class = UserClass::find($user_class_id);
        $course_id = $user_class->course_id;

        $course = Course::find($course_id);

        return $course->course_name;
    }

    private function getUserClassLocation($user_class_id) {
        $user_class = UserClass::find($user_class_id);
        $location_id = $user_class->location_id;

        $location = Location::find($location_id);

        return $location->location_name;
    }

    private function getTodayDate() {
        $today = Carbon::now()->setTimezone('Asia/Jakarta')->toDateString();
        return $today;
    }
}
