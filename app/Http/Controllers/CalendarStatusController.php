<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CalendarStatus;

class CalendarStatusController extends Controller
{
    public function getAllCalendarStatus(){
        $calendarStatuses = CalendarStatus::all();
        return response()->json([
            'status' => 200,
            'message' => "OK",
            'data' => $calendarStatuses
        ]);
    }

    public function getCalendarStatus(CalendarStatus $calendarStatus){
        return response()->json([
            'status' => 200,
            'message' => "OK",
            'data' => $calendarStatus
        ]);
    }
}
