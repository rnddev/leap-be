<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Config;
use Validator;

class ConfigController extends Controller
{
    public function index(){
        $config = Config::first();
        return response()->json([
            'status' => 200,
            'message' => 'OK',
            'data' => $config
        ]);
    }

    public function updateConfig(Request $request){
        $validator = Validator::make($request->all(),[
            'period' => 'required|numeric',
            'term_id' => 'required|numeric|exists:terms,id|min:1',
        ]);

        if($validator->fails()){
            return response()->json($validator->messages(), 400);
        }

        $config = Config::first();
        $message = "Data succesfully updated";
        if($config==null){
            $config = new Config;
            $message = "Data successfully created";
        }
        $config->period = $request->period;
        $config->term_id = $request->term_id;
        $config->save();

        return response()->json([
            'status' => 200,
            'message' => $message,
            'error' => ""
        ]);
    }
}
