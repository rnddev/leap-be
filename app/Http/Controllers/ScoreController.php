<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Score;
use Validator;

class ScoreController extends Controller
{
    public function createScore(Request $request){
        $validator = Validator::make(request()->all(),[
            'class_detail_id' => 'required|integer|min:1',
            'mid_score' => 'integer|between:0,100',
            'final_score' => 'integer|between:0,100'
        ]);

        if($validator->fails()){
            return response()->json($validator->messages(),400);
        }

        $score = new Score;
        $score->class_detail_id = $request->class_detail_id;
        $score->assignment_score = $request->assignment_score;
        $score->mid_score = $request->mid_score;
        $score->final_score = $request->final_score;
        $score->save();

        return response()->json([
            'status' => 201,
            'message' => 'OK',
            'error' => ""
        ]);
    }

    public function getAllScore(){
        $scores = Score::select('class_detail_id','mid_score','final_score')->get();
        return response()->json([
            'status' => 200,
            'message' => "OK",
            'data' => $scores
        ]);
    }

    public function getScore(Score $score){
        $score = Score::select('class_detail_id','mid_score','final_score')->where('id',$score->id)->get();
        return response()->json([
            'status' => 200,
            'message' => "OK",
            'data' => $score
        ]);
    }

    public function updateScore(Request $request, Score $score){
        $validator = Validator::make(request()->all(),[
            'class_detail_id' => 'integer|min:1',
            'mid_score' => 'integer|between:0,100',
            'final_score' => 'integer|between:0,100'
        ]);

        if($validator->fails()){
            return response()->json($validator->messages(),400);
        }


        $score->class_detail_id = $request->class_detail_id == null? $score->class_detail_id : $request->class_detail_id ;
        $score->assignment_score = $request->assignment_score == null? $score->assignment_score : $request->assignment_score ;
        $score->mid_score = $request->mid_score == null? $score->mid_score : $request->mid_score ;
        $score->final_score = $request->final_score == null? $score->final_score : $request->final_score ;
        $score->save();

        return response()->json([
            'status' => 200,
            'message' => "Data updated",
        ]);

    }

    public function deleteScore(Score $score){
        $score->delete();
        return response()->json([
            'status' => 200,
            'message' => "Data deleted successfuly"
        ]);
    }
}
