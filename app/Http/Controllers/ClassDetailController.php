<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\ClassDetail;
use App\Absence;
use Validator;

class ClassDetailController extends Controller
{
    public function countMemberAttandences(){
        $memberId = auth()->user()->id;
        $count = 0;
        $classDetails = ClassDetail::with([
            'absence' => function($query) {
                return $query->where('absence', true);
            }
        ])->where('member_id',$memberId)->get();

        foreach($classDetails as $classDetail) {
            $count+= $classDetail->absence->count();
        }

        $status = 200;
        $message = "OK";
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $count
        ]);
    }
    public function createClassDetail(Request $request){
        $validator = Validator::make($request->all(),[
            'class_id' => 'required|exists:user_classes,id|integer',
            'member_id' => 'required|exists:users,id|integer'
        ]);
        if($validator->fails()){
            return response()->json($validator->messages(),400);
        } 

        $classDetail = new ClassDetail;
        $classDetail->user_class_id = $request->class_id;
        $memberRegistered = DB::table('class_details')->where('user_class_id',$request->class_id)->count();
        if($classDetail->userClass->capacity > $memberRegistered){
            $classDetail->member_id = $request->member_id;
            $classDetail->save();
            $SCORE_DATA = ['mid_score'=>0,'final_score'=>0];
            $SUBMIT_ASSIGNMENT = [' mid_exam_file'=>"",' final_exam_file'=>""];
            $classDetail->score()->create($SCORE_DATA);
            $classDetail->submittedAssignment()->create($SUBMIT_ASSIGNMENT);
            $classDetail->score;
            $classDetail->submittedAssignment;
            $classSchedules = DB::table('class_schedules')->where('user_class_id',$classDetail->user_class_id)->get();
            foreach($classSchedules as $classSchedule){
                $classDetail->absence()->create(['absence'=>0, 'class_detail_id'=>$classDetail->id, 'class_schedule_id'=>$classSchedule->id]);
            }
            $status = 201;
            $message = "OK";
            $error = "";
        } else {
            $status = 400;
            $message = "fail created student";
            $error = "No Capacity For the student";
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'error' => $error
        ]);
    }

    public function updateClassDetail(ClassDetail $classDetail,Request $request){
        $validator = Validator::make($request->all(),[
            'class_id' => 'exists:user_classes,id|integer'
        ]);
        if($validator->fails()){
            return response()->json($validator->messages(),400);
        } 
        
        $classDetailNew = new ClassDetail;
        $classDetailNew->user_class_id = $request->class_id==null?$classDetail->class_id:$request->class_id;
        $memberRegistered = DB::table('class_details')->where('user_class_id',$request->class_id)->count();
        if($classDetailNew->userClass->capacity > $memberRegistered){
            $classDetailNew->member_id = $classDetail->member_id;
            $classDetailNew->save();
            $SCORE_DATA = ['mid_score'=>0,'final_score'=>0];
            $SUBMIT_ASSIGNMENT = [' mid_exam_file'=>"",' final_exam_file'=>""];
            $classDetailNew->score()->create($SCORE_DATA);
            $classDetailNew->submittedAssignment()->create($SUBMIT_ASSIGNMENT);
            $classDetailNew->score;
            $classDetailNew->submittedAssignment;
            $classSchedules = DB::table('class_schedules')->where('user_class_id',$classDetailNew->user_class_id)->get();
            foreach($classSchedules as $classSchedule){
                $classDetail->absence()->create(['absence'=>0, 'class_detail_id'=>$classDetailNew->id, 'class_schedule_id'=>$classSchedule->id]);
            }
            $classDetail->delete();
            $status = 200;
            $message = "OK";
            $error = "";
        } else {
            $status = 400;
            $message = "fail Updated student";
            $error = "No Capacity For the student";
        }

        return response()->json([
            'status'=>$status,
            'message'=>$message,
            'error' => $error
        ]);
    }

    public function deleteClassDetail(ClassDetail $classDetail){
        $classDetail->delete();
        return response()->json([
            "status"=>200,
            "message"=>"data successfully deleted"
        ]);
    }
}
