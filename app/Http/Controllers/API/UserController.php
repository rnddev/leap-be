<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use DB;
use Auth;

class UserController extends Controller
{
    public function login()
    {
        $credentials = [
            'email' => request('email'),
            'password' => request('password')
        ];
        if (auth()->attempt($credentials)) {
            $success['token'] = auth()->user()->createToken('GojekToken')->accessToken;
            $success['expired_at'] = Carbon::now()->addHours(1)->toRfc7231String();
            $success['role'] = auth()->user()->role->role_name;
            return response()->json(['success' => $success], 200);
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }

    public function register()
    {
        $data = Validator::make(request()->all(), [
            'name' => ['required', 'string'],
            'user_id' => ['required'],
            'email' => ['required', 'string', 'email', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'date_of_birth' => ['required', 'date'],
            'major_id' => ['required', 'numeric'],
            'gender' => ['required'],
            'phone_number' => ['required'],
            'instagram_id' => ['required'],
            'line_id' => ['required'],
            'role_id' => ['required', 'numeric'],
        ]);
        if ($data->fails()) {
            return response()->json(['error' => $data->errors()], 201);
        }
        $input = request()->all();
        $input['password'] = bcrypt($input['password']);

        $user = User::create($input);

        $success['token'] = $user->createToken('GojekToken')->accessToken;
        $success['name'] = $user->name;
        return response()->json(['success' => $success], 200);
    }

    public function details()
    {
        $user_id = auth()->user()->id;
        $details = DB::table('users')
                    ->join('majors', 'users.major_id', '=', 'majors.id')
                    ->select('users.*', 'majors.major_name')
                    ->where('users.id', $user_id)->first(); 
        return response()->json(['success' => $details], 200);
    }

    public function logout(Request $request){
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    public function changeEmail()
    {
        $validator = Validator::make(request()->all(), [
            'email' => ['required', 'string', 'email', 'unique:users'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $input = request()->all();
        $user = auth()->user();
        $user->email = $input['email'];
        $user->save();
        return response()->json(['success' => "Data Updated"], 200);
    }

    public function changePassword()
    {
        $validator = Validator::make(request()->all(), [
            'old_password' => ['required'],
            'new_password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $input = request()->all();
        $input['new_password'] = bcrypt($input['new_password']);
        $user = auth()->user();

        $check  = Auth::guard('web')->attempt([
            'email' => $user->email,
            'password' => $input['old_password']
        ]);

        if($check) {
            $user->password = $input['new_password'];
            $user->save();
        } else {
            return response()->json(['success' => "Wrong Old Password!"], 200);
        }

        return response()->json(['success' => "Password Updated"], 200);
    }
}
