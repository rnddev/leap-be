<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Curriculum;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Storage;
use Validator;

class CurriculaController extends Controller
{
    public function getAllCurricula()
    {
        $curricula = DB::table('curricula')
            ->join('courses', 'curricula.course_id', '=', 'courses.id')
            ->join('terms', 'curricula.term_id', '=', 'terms.id')
            ->select('curricula.*', 'courses.course_name', 'terms.term_name')->get();

        return response()->json([
            'status' => 200,
            'message' => "OK",
            'data' => $curricula
        ]);
    }

    public function storeCurriculum(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'course_id' => 'required|integer|min:1|exists:courses,id',
            'term_id' => 'required|integer|min:1|exists:terms,id',
            'meeting_name' => 'required|string|between:1,100',
            'meeting' => 'required|integer|between:1,30',
            'material_file' => 'required|mimes:pdf,doc,docx,ppt,pptx,xls,xlsx,odt,jpeg,jpg,png'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $material_file_name = $request->course_id .
            '/' . $request->term_id .
            '/' . $request->meeting .
            '/' . $request->course_id .
            '_' . $request->term_id .
            '_' . $request->meeting .
            '_' . $request->meeting_name .
            '_' . Carbon::now()->timestamp . '.' . $request->material_file->extension();

        $curriculum = new Curriculum;
        $curriculum->course_id = $request->course_id;
        $curriculum->term_id = $request->term_id;
        $curriculum->meeting_name = $request->meeting_name;
        $curriculum->meeting = $request->meeting;
        $curriculum->material = $material_file_name;

        Storage::disk('public')->put($material_file_name, file_get_contents($request->material_file));

        $curriculum->save();
        return response()->json([
            'status' => 201,
            'message' => 'OK',
            'error' => ""
        ]);
    }

    public function storeCurricula(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            "course_id" => "required|array",
            "term_id" => "required|array",
            "meeting_name" => "required|array",
            "meeting" => "required|array",
            "material_file" => "required|array",
            'course_id.*' => 'required|integer|min:1|exists:courses,id',
            'term_id.*' => 'required|integer|min:1|exists:terms,id',
            'meeting_name.*' => 'required|string|between:1,100',
            'meeting.*' => 'required|integer|between:1,30',
            'material_file.*' => 'required|mimes:pdf,doc,docx,ppt,pptx,xls,xlsx,odt,jpeg,jpg,png'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $c = count($request->course_id);

        for ($i = 0; $i < $c; $i++) {
            $material_file_name = $request->course_id[$i] .
                '/' . $request->term_id[$i] .
                '/' . $request->meeting[$i] .
                '/' . $request->course_id[$i] .
                '_' . $request->term_id[$i] .
                '_' . $request->meeting[$i] .
                '_' . $request->meeting_name[$i] .
                '_' . Carbon::now()->timestamp . '.' . $request->material_file[$i]->extension();

            $curriculum = new Curriculum;
            $curriculum->course_id = $request->course_id[$i];
            $curriculum->term_id = $request->term_id[$i];
            $curriculum->meeting_name = $request->meeting_name[$i];
            $curriculum->meeting = $request->meeting[$i];
            $curriculum->material = $material_file_name;

            Storage::disk('public')->put($material_file_name, file_get_contents($request->material_file[$i]));

            $curriculum->save();
        }

        return response()->json([
            'status' => 201,
            'message' => 'OK',
            'error' => ""
        ]);
    }


    public function getCurricula(Curriculum $curriculum)
    {
        $curricula = DB::table('curricula')
            ->join('courses', 'curricula.course_id', '=', 'courses.id')
            ->join('terms', 'curricula.term_id', '=', 'terms.id')
            ->select('curricula.*', 'courses.course_name', 'terms.term_name')
            ->where('curricula.id', $curriculum->id)->first();

        return response()->json([
            'status' => 200,
            'message' => "OK",
            'data' => $curricula
        ]);
    }

    public function updateCurriculum(Request $request, Curriculum $curriculum)
    {
        $validator = Validator::make(request()->all(), [
            'course_id' => 'integer|min:1|exists:courses,id',
            'term_id' => 'integer|min:1|exists:terms,id',
            'meeting_name' => 'string|between:1,100',
            'meeting' => 'integer|between:1,30',
            'material_file' => 'mimes:pdf,doc,docx,ppt,pptx,xls,xlsx,odt,jpeg,jpg,png'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $curriculum->course_id = $request->course_id == null ? $curriculum->course_id : $request->course_id;
        $curriculum->term_id = $request->term_id == null ? $curriculum->term_id : $request->term_id;
        $curriculum->meeting_name = $request->meeting_name == null ? $curriculum->meeting_name : $request->meeting_name;
        $curriculum->meeting = $request->meeting == null ? $curriculum->meeting : $request->meeting;

        $check = FALSE;
        if ($request->hasFile('material_file')) {
            Storage::disk('public')->delete($curriculum->material);

            $check = TRUE;

            $material_file_name = $curriculum->course_id .
                '/' . $curriculum->term_id .
                '/' . $curriculum->meeting .
                '/' . $curriculum->course_id .
                '_' . $curriculum->term_id .
                '_' . $curriculum->meeting .
                '_' . $curriculum->meeting_name .
                '_' . Carbon::now()->timestamp . '.' . $request->material_file->extension();
            $curriculum->material = $material_file_name;

            Storage::disk('public')->put($material_file_name, file_get_contents($request->material_file));
        }

        if (!$check) {
            $curriculum->material = $curriculum->material;
        }

        $curriculum->save();

        return response()->json([
            'status' => 200,
            'message' => "Data updated",
        ]);
    }

    public function updateCurricula(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            "id" => "required|array",
            "course_id" => "array",
            "term_id" => "array",
            "meeting_name" => "array",
            "meeting" => "array",
            "material_file" => "array",
            'id.*' => 'required|integer|min:1',
            'course_id.*' => 'integer|min:1|exists:courses,id',
            'term_id.*' => 'integer|min:1|exists:terms,id',
            'meeting_name.*' => 'string|between:1,100',
            'meeting.*' => 'integer|between:1,30',
            'material_file.*' => 'mimes:pdf,doc,docx,ppt,pptx,xls,xlsx,odt,jpeg,jpg,png'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $c = count($request->id);

        for ($i = 0; $i < $c; $i++) {
            $curriculum = Curriculum::where('id', $request->id[$i])->first();
            $curriculum->course_id = isset($request->course_id[$i]) ? $request->course_id[$i] : $curriculum->course_id;
            $curriculum->term_id = isset($request->term_id[$i]) ? $request->term_id[$i] : $curriculum->term_id;
            $curriculum->meeting_name = isset($request->meeting_name[$i]) ? $request->meeting_name[$i] : $curriculum->meeting_name;
            $curriculum->meeting = isset($request->meeting[$i]) ? $request->meeting[$i] : $curriculum->meeting;

            $check = FALSE;
            if ($request->hasFile('material_file.'.$i)) {
                Storage::disk('public')->delete($curriculum->material);

                $check = TRUE;

                $material_file_name = $curriculum->course_id .
                    '/' . $curriculum->term_id .
                    '/' . $curriculum->meeting .
                    '/' . $curriculum->course_id .
                    '_' . $curriculum->term_id .
                    '_' . $curriculum->meeting .
                    '_' . $curriculum->meeting_name .
                    '_' . Carbon::now()->timestamp . '.' . $request->material_file[$i]->extension();
                $curriculum->material = $material_file_name;

                Storage::disk('public')->put($material_file_name, file_get_contents($request->material_file[$i]));
            }



            if (!$check) {
                $curriculum->material = $curriculum->material;
            }

            $curriculum->save();
        }



        return response()->json([
            'status' => 200,
            'message' => "Data updated",
        ]);
    }

    public function deleteCurricula(Curriculum $curriculum)
    {
        Storage::disk('public')->delete($curriculum->material);
        $curriculum->delete();
        return response()->json([
            'status' => 200,
            'message' => "Data deleted successfuly"
        ]);
    }

    public function getCurriculabyCourse($course_id)
    {
        $curricula = DB::table('curricula')
            ->join('courses', 'curricula.course_id', '=', 'courses.id')
            ->join('terms', 'curricula.term_id', '=', 'terms.id')
            ->select('curricula.*', 'courses.course_name', 'terms.term_name')
            ->where('curricula.course_id', $course_id)->get();
        return response()->json([
            'status' => 200,
            'message' => "OK",
            'data' => $curricula
        ]);
    }
}
