<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    protected $fillable = ['term_name'];

    public function userClass() {
        return $this->hasMany('App\UserClass');
    }

    public function config() {
        return $this->hasOne('App\Config');
    }

    public function academicCalendar() {
        return $this->hasMany('App\AcademicCalendar');
    }
    
    public function score(){
        return $this->hasMany('App\Score');
    }
}
