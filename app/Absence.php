<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Absence extends Model
{
    protected $fillable=['class_detail_id','class_schedule_id','absence'];

    public function classSchedule(){
        return $this->belongsTo('App\ClassSchedule');
    }

    public function classDetail(){
        return $this->belongsTo('App\ClassDetail');
    }
}
