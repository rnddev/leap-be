<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassDetail extends Model
{
    protected $fillable = ['user_class_id','member_id'];

    public function user(){
        return $this->belongsTo('App\User','member_id');
    }

    public function userClass(){
        return $this->belongsTo('App\UserClass','user_class_id');
    }

    public function submittedAssignment(){
        return $this->hasOne('App\SubmittedAssignment');
    }

    public function absence(){
        return $this->hasMany('App\Absence');
    }

    public function score(){
        return $this->hasOne('App\Score');
    }
}
