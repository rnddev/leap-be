<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    protected $fillable = ['begin_time','end_time'];

    public function classSchedule() {
        return $this->hasMany('App\ClassSchedule');
    }

    public function userClass(){
        return $this->hasMany('App\UserClass','master_shift_id','id');
    }

    
}
