<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $fillable = ['period','term_id'];

    public function term(){
        return $this->belongsTo('App\Term');
    }
}
