<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcademicCalendar extends Model
{
    protected $fillable = ['period','term_id','begin_date','end_date','description','calendar_status_id'];

    public function term(){
        return $this->belongsTo('App\Term');
    }

    public function calendarStatus(){
        return $this->belongsTo('App\CalendarStatus');
    }
}
